package com.webanalytics;
import com.webanalytics.domain.Traffic;
import com.webanalytics.domain.TrafficSummary;

import java.util.Date;
import java.util.List;
import java.text.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.webanalytics.repository.TrafficRepository;

@RestController
public class TrafficRestController {

    @Autowired
    private TrafficRepository repo;

    @RequestMapping(value = {"/getAllTraffic"}, method=RequestMethod.GET)
    public List<Traffic> getAll() {
        return repo.findAll();
    }

    @RequestMapping(value = {"/getTrafficByDate"}, method=RequestMethod.GET)
    public List<Traffic> getAllbyDate(@RequestParam(value="dateLong") Long dateLong) {
        Date date = new Date(dateLong);
        Format format = new SimpleDateFormat("yyyy MM dd HH:mm:ss");
        return repo.findByDate(date);
    }

    @RequestMapping(value = {"/getTrafficBySite"}, method=RequestMethod.GET)
    public List<Traffic> getAllbyWebite(@RequestParam(value="webSite") String webSite) {
        return repo.findByWebsite(webSite);
    }

    @RequestMapping(value = {"/getChartData"}, method=RequestMethod.GET)
    public List<TrafficSummary> getChartData() {
        return repo.getChartData();
    }

    @RequestMapping(value = {"/getVisitsByDate"}, method=RequestMethod.GET)
    public List<TrafficSummary> getVisitsByDate() {
        return repo.getVisitsByDate();
    }

    @RequestMapping(value = {"/getChartDataByDate"}, method=RequestMethod.GET)
    public List<TrafficSummary> getChartDataByDate(@RequestParam(value="visitDate") Long visitDate) {
        Date date = new Date(visitDate);
        return repo.getChartDataByDate(date);
    }

    private void displayPageCommon(){
        //shared code
    }

}