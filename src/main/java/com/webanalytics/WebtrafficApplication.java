package com.webanalytics;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebtrafficApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebtrafficApplication.class, args);
	}
}
