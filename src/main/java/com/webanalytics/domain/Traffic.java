package com.webanalytics.domain;

import javax.persistence.*;
import java.util.Date;


@Entity
public class Traffic {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private Date date;
    private String website;
    private Long visits;

    public Traffic() {}

    public Traffic(Date date, String website, Long visits) {
        this.date = date;
        this.website = website;
        this.visits = visits;
    }

    public Traffic( String website, Long visits) {
        this.website = website;
        this.visits = visits;
    }

    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    public void setDate(Date date) {
        this.date = date;
    }
    public Date getDate() {
        return date;
    }

    public void setWebsite(String website) { this.website = website; }
    public String getWebsite() {  return website; }

    public void setVisits(Long visits) { this.visits = visits; }
    public Long getVisits() { return visits; }


}
