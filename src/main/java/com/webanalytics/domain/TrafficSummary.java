package com.webanalytics.domain;
import java.util.Date;


public class TrafficSummary {
    public String website;
    public Long totalVisits;
    public Date visitDate;

    public TrafficSummary(String website, Long totalVisits) {
        this.website = website;
        this.totalVisits = totalVisits;
    }

    public TrafficSummary(Date date, Long totalVisits) {
        this.visitDate = date;
        this.totalVisits = totalVisits;
    }
}
