package com.webanalytics.repository;
import org.springframework.data.jpa.repository.Query;

import org.springframework.data.repository.CrudRepository;
import com.webanalytics.domain.Traffic;
import com.webanalytics.domain.TrafficSummary;

import java.util.Date;
import java.util.List;

public interface TrafficRepository extends CrudRepository<Traffic, Integer> {

    List<Traffic> findAll();
    List<Traffic> findByDate(Date date);
    List<Traffic> findByWebsite (String website);

    @Query(value ="SELECT new com.webanalytics.domain.TrafficSummary(v.website as website , sum(v.visits) as totalVists) from Traffic v group by v.website order by totalVists DESC")
    List<TrafficSummary> getChartData ();

    @Query(value ="SELECT new com.webanalytics.domain.TrafficSummary(v.date as date , sum(v.visits) as totalVists) from Traffic v ")
    List<TrafficSummary> getVisitsByDate ();

    @Query(value ="SELECT new com.webanalytics.domain.TrafficSummary(v.website as website , sum(v.visits) as totalVists) from Traffic v where v.date = ?1 group by website ")
    List<TrafficSummary> getChartDataByDate (Date date );

}

