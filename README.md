# README #

The API repository is at https://bitbucket.org/krishnendu_krishnan/webtrafficanalytics
and Web front end is at https://bitbucket.org/krishnendu_krishnan/webfrontend

Application is hosted at http://54.243.8.123/

Tools used
----------
* Database - MySQL
* API Layer - Spring boot

Front end
----------
* React JS
* Semantic UI


I haven't been able to build the application to the full potential in this time, but i will be improving it in the coming days

To create this app as cloud native, I would

Deploy the app to Docker containers and define them in docker compose for each environment
Create micro services (Since the scope of this app is small, may be one micro service for traffic data). If there are additional, would detach them logically from each other and would use a discovery service like consul 
Purpose is to be able to modify each micro service without impacting other. Logically group micro services together. 

Use git commit hooks to trigger a CI process with Jenkins/bamboo to build and deploy the Spring app to a Tomcat server running in a docker container. The docker container will be spawned during deploy time.

Isolate the configuration and check it in separately from environment

* Database connection info,
* API end points
* General setting such as Host names etc

I would re factor lot of code to make it modular, especially front end. There are bugs Did not get time to organize it.